data "external" "download_git_hub_example" {
  count   = var.upload_test ? 1 : 0
  program = ["bash", "${path.module}/sample_img/download_img.sh"]
  query = {
    url           = "https://github.com/ianare/exif-samples/raw/master/jpg/hdr/iphone_hdr_YES.jpg"
    dest_filename = "${path.module}/sample_img/example.jpg"
  }
}

resource "aws_s3_bucket_object" "test_image_upload" {
  count  = var.upload_test ? 1 : 0
  bucket = module.s3_bucket_a.s3_bucket_id
  key    = "test/img.jpg"
  source = "${path.module}/${data.external.download_git_hub_example[0].result.file}"
  etag   = filemd5("${path.module}/${data.external.download_git_hub_example[0].result.file}")
  depends_on = [
    module.lambda_function,
    aws_lambda_permission.allow_bucket_trigger,
    data.external.download_git_hub_example
  ]
}

