module "s3_bucket_a" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = "${lookup(local.aws-profile, terraform.workspace)}-${lower(lookup(local.workspace, terraform.workspace))}-my-s3-bucket-a"
  acl    = "private"

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = aws_kms_key.this.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  tags = local.common_tags
}


resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = module.s3_bucket_a.s3_bucket_id

  lambda_function {
    lambda_function_arn = module.lambda_function.lambda_function_arn
    events              = ["s3:ObjectCreated:Put", "s3:ObjectCreated:Post", "s3:ObjectCreated:Copy"]
    #filter_prefix       = "/"
    filter_suffix = ".jpg"
  }

  depends_on = [
    module.s3_bucket_a,
    module.lambda_function
  ]
}

module "s3_bucket_b" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = "${lookup(local.aws-profile, terraform.workspace)}-${lower(lookup(local.workspace, terraform.workspace))}-my-s3-bucket-b"
  acl    = "private"

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = aws_kms_key.this.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  tags = local.common_tags
}



