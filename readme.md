[![LinkedIn][linkedin-shield]][linkedin-url]

## About The Project

This repo creates one source bucket_a, a destination bucket_b, and a lambda function in python that will clean exif metadata from the source image and save in the destination bucket the clean file.

### Prerequisites

in your machine you will probably want to have installed,
The below links are for Linux would be preferable to verify the sources if you are running the below curl to install the software:

* [terraform]  or [tfswich]
```sh
curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | bash
```

### Installation

1. Install the prerequisites
2. Clone the repo & review the variables.tf file:
   
   ```sh
   git clone https://bitbucket.org/anninocloudclusters/exif_metadata_clean.git 
   vi variables.tf
   ```
    edit this block to reflect your configuration, this will be as well reused in the resource names, to create unique resources:
   ```sh
     aws-profile = {
    "default"    = "<annino-cloud> <-- change it to your aws profile name"
    "prod-eu"    = "<annino-cloud> <-- change it to your aws profile name"
    ...
    }
   ```

3. Initialize terraform
   
   ```sh
   terraform init
   ```

4. Plan
   
   ```sh
   terraform plan
   ```

5. Apply the initial test under the default workspace

   ```sh
   terraform apply
   ```

6. after the first apply you might want to test a file upload, please use the command below:
   
   ```sh
   time terraform apply -var upload_test=true -auto-approve && date
   terraform output user_a
   terraform output user_b
   ```
   this test upload will happen using the aws profile used by terraform, you can configure your AWS CLI with the users access_key secret to try the user

7. now that you have build and tested the code, you might want to create to the production workspace to deliver this with prod naming, let's switch workspace and run a terraform apply:

   ```sh
   terraform workspace new prod-eu
   time terraform apply -var upload_test=true -auto-approve && date
   ```   
   this will create resources with different name

8. you can test the users after configuring the users in your aws cli:

   ```sh
   export tf_aws_profile_name=annino-cloud #<-- change this to your profile
   export tf_workspace=uat-eu #<-- change this to the terraform workspace you decided to test
   export aws_user_a_profile_name=user_a #<-- change this to your profile
   export aws_user_b_profile_name=user_b #<-- change this to your profile
   export aws_region=eu-central-1

   #USERA
   aws s3 --profile $aws_user_a_profile_name --region $aws_region ls s3://$tf_aws_profile_name-$tf_workspace-my-s3-bucket-a
   aws s3 --profile $aws_user_a_profile_name --region $aws_region cp s3://$tf_aws_profile_name-$tf_workspace-my-s3-bucket-a/test/img.jpg .
   aws s3 --profile $aws_user_a_profile_name --region $aws_region cp img.jpg s3://$tf_aws_profile_name-$tf_workspace-my-s3-bucket-a/test/img.jpg
   aws s3 --profile $aws_user_a_profile_name --region $aws_region ls s3://$tf_aws_profile_name-$tf_workspace-my-s3-bucket-b

   #USERB
   aws s3 --profile $aws_user_b_profile_name --region $aws_region ls s3://$tf_aws_profile_name-$tf_workspace-my-s3-bucket-b
   aws s3 --profile $aws_user_b_profile_name --region $aws_region cp img.jpg s3://$tf_aws_profile_name-$tf_workspace-my-s3-bucket-b/test/img.jpg
   aws s3 --profile $aws_user_b_profile_name --region $aws_region cp s3://$tf_aws_profile_name-$tf_workspace-my-s3-bucket-b/test/img.jpg . 
   ```

9. (Optional) Destroy all the resources for both workspaces, after you emptied the bucket_b (removing all the objects)

   ```sh
   time terraform destroy -auto-approve && date
   terraform workspace new default
   time terraform destroy -auto-approve && date
   ```

## License

Distributed under the GNU GPL License. 

## Contact

Giovanni Annino - [@gannino](https://giovannino.net) - giovanni.annino@gmail.com

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/gannino/
