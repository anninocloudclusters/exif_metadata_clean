#!/usr/bin/env bash

set -e

eval "$(jq -r '@sh "url=\(.url) dest_filename=\(.dest_filename)"')"

curl -L $url -o $dest_filename 2>&1 | logger -t download_image

echo "{\"file\" : \"$dest_filename\",\"success\" : \"$?\"}"
