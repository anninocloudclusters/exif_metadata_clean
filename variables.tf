###########################
#  Main project - Var     #
###########################
data "aws_region" "current" {
}


data "aws_caller_identity" "current" {
}


variable "name" {
  default = "annino"
}
variable "project" {
  default = "cloud"
}

variable "upload_test" {
  default = false
}

locals {
  project_name   = "${lookup(local.workspace, terraform.workspace)} GE exercise AWS-Python-Terraform"
  url            = "https://giovannino.net"
  url_developers = "https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/index.html"
  # Common tags to be assigned to all resources
  common_tags = {
    project_name        = local.project_name
    environment         = lookup(local.workspace, terraform.workspace)
    customer_name       = "SHARED-PRODUCT"
    permanent           = true
    expires             = "never"
    scheduled_hours     = false
    deployment_method   = "IaC"
    deployment_doc_b64  = base64encode(local.url)
    development_doc_b64 = base64encode(local.url_developers)
    bamboo_managed      = false
    owner_contact       = "giovanni@annino.cloud"
    technical_contact   = "giovanni@annino.mobi"
    # schedule_start = 0-23 - the startup hour (UTC)
    # schedule_stop = 0-23 - the shutdown hour (UTC)
    # schedule_days = comma delimited list of days to run 1,2,3,4,5,6,7 (1= Monday, 7=Sunday)
  }

  workspace = {
    "default"    = "BUILD"
    "preprod"    = "PREPROD"
    "sit"        = "SIT"
    "qa"         = "QA"
    "uat"        = "UAT"
    "fat"        = "FAT"
    "prod-us"    = "PROD-US"
    "preprod-us" = "PREPROD-US"
    "sit-us"     = "SIT-US"
    "qa-us"      = "QA-US"
    "uat-us"     = "UAT-US"
    "fat-us"     = "FAT-US"
    "prod-eu"    = "PROD-EU"
    "preprod-eu" = "PREPROD-EU"
    "sit-eu"     = "SIT-EU"
    "qa-eu"      = "QA-EU"
    "uat-eu"     = "UAT-EU"
    "fat-eu"     = "FAT-EU"
  }

  aws-profile = {
    "default"    = "annino-cloud"
    "prod"       = "annino-cloud"
    "preprod"    = "annino-cloud"
    "sit"        = "annino-cloud"
    "qa"         = "annino-cloud"
    "uat"        = "annino-cloud"
    "fat"        = "annino-cloud"
    "prod-us"    = "annino-cloud"
    "preprod-us" = "annino-cloud"
    "sit-us"     = "annino-cloud"
    "qa-us"      = "annino-cloud"
    "uat-us"     = "annino-cloud"
    "fat-us"     = "annino-cloud"
    "prod-eu"    = "annino-cloud"
    "preprod-eu" = "annino-cloud"
    "sit-eu"     = "annino-cloud"
    "qa-eu"      = "annino-cloud"
    "uat-eu"     = "annino-cloud"
    "fat-eu"     = "annino-cloud"
  }



  geography = {
    "default"    = "EU"
    "prod"       = "AP"
    "preprod"    = "EU"
    "sit"        = "US"
    "qa"         = "US"
    "uat"        = "US"
    "fat"        = "US"
    "prod-us"    = "US"
    "preprod-us" = "US"
    "sit-us"     = "US"
    "qa-us"      = "US"
    "uat-us"     = "US"
    "fat-us"     = "US"
    "prod-eu"    = "EU"
    "preprod-eu" = "EU"
    "sit-eu"     = "EU"
    "qa-eu"      = "EU"
    "uat-eu"     = "EU"
    "fat-eu"     = "EU"
  }
}

variable "geography_rega" {
  type = map(any)
  default = {
    EU = "eu-central-1"
    US = "us-east-1"
  }
}

variable "geography_regb" {
  type = map(any)
  default = {
    EU = "eu-west-1"
    US = "us-west-1"
  }
}


variable "reg_short_name" {
  type = map(any)
  default = {
    eu-central-1 = "REGA"
    eu-west-1    = "REGB"
    us-east-1    = "REGA"
    us-west-1    = "REGB"
  }
}
