provider "aws" {
  region  = lookup(var.geography_rega, lookup(local.geography, terraform.workspace))
  profile = lookup(local.aws-profile, terraform.workspace)
}