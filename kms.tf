# would be better using something like this:
# https://github.com/k9securityio/terraform-aws-kms-key

resource "aws_kms_key" "this" {
  description             = "a kms key to encrypt s3 data"
  deletion_window_in_days = 10
  enable_key_rotation     = true
  tags                    = local.common_tags
}

resource "aws_kms_alias" "this" {
  name          = "alias/${terraform.workspace}_${data.aws_region.current.name}_s3_kms_key"
  target_key_id = aws_kms_key.this.key_id
}
