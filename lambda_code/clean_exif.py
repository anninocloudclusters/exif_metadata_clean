#!/usr/bin/env python3

import json
import logging
import io
import boto3
import os
import time
from pathlib import Path
from botocore.exceptions import ClientError

# import sys
# import subprocess
# subprocess.call('pip install ratelimiter -t /tmp/ --no-cache-dir'.split(), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
# subprocess.call('pip install exif -t /tmp/ --no-cache-dir'.split(), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
# sys.path.insert(1, '/tmp/')

from ratelimiter import RateLimiter
from exif import Image


s3 = boto3.client('s3')
s3_resource = boto3.resource('s3')

logger = logging.getLogger(__name__)

def limited(until):
    duration = int(round(until - time.time()))
    print('Rate limited, sleeping for {:d} seconds'.format(duration))

rate_limiter = RateLimiter(max_calls=1, period=1, callback=limited)


def read_lambda_input(event):
    event_type = event['Records'][0]['eventName']
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    file_name = event['Records'][0]['s3']['object']['key']
    return file_name, bucket_name, event_type

def read_s3_file(file_name, bucket):
    s3_object = s3_resource.Object(bucket, file_name)
    with io.BytesIO() as f:
        s3_object.download_fileobj(f)
        f.seek(0)
        content = f.read()
    return content

def clean_exif(source_content):
    byte_content = bytes(source_content)
    image = Image(byte_content)
    clean_img = image.delete_all()
    no_exif_image = image.get_file()
    return no_exif_image

def copy_source_to_destination(event):
    source_file, source_bucket, event_type = read_lambda_input(event)
    print('New file uploaded to source S3 bucket ' + source_file + ' using method ' + event_type )
    destination_bucket = os.environ['DEST_BUCKET']
    source_content = read_s3_file(source_file, source_bucket)
    clean_image = clean_exif(source_content)
    with rate_limiter:
        s3.put_object(Body=clean_image, Bucket=destination_bucket, Key=source_file, ContentType='image/jpeg')
    return(source_file + ' has been cleaned and saved in the destination bucket ' + destination_bucket)

def lambda_handler(event, context):
    print(copy_source_to_destination(event))
