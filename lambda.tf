resource "aws_iam_policy" "lambda_img_metadata" {
  name        = "${lower(terraform.workspace)}_lambda_img_metadata_policy"
  description = "A policy for Lambda to access s3 and KMS"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "${module.s3_bucket_a.s3_bucket_arn}/*",
                "${module.s3_bucket_a.s3_bucket_arn}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject"
            ],
            "Resource": [
                "${module.s3_bucket_b.s3_bucket_arn}/*",
                "${module.s3_bucket_b.s3_bucket_arn}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt",
                "kms:ReEncrypt*",
                "kms:GenerateDataKey*",
                "kms:DescribeKey"
            ],
            "Resource": [
                "${aws_kms_key.this.arn}"
            ]
        }        
    ]
}
EOF
}

resource "aws_lambda_permission" "allow_bucket_trigger" {
  statement_id  = "AllowExecutionFromS3Bucket1"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda_function.lambda_function_name
  principal     = "s3.amazonaws.com"
  source_arn    = module.s3_bucket_a.s3_bucket_arn

  depends_on = [
    module.s3_bucket_a,
    module.lambda_function
  ]
}



module "lambda_function" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = "${lookup(local.aws-profile, terraform.workspace)}-${lower(lookup(local.workspace, terraform.workspace))}-lambda-img-metadata"
  description   = "a function to remove exif metadata from images"
  handler       = "clean_exif.lambda_handler"
  runtime       = "python3.8"

  source_path = "${path.module}/lambda_code/clean_exif.py"

  environment_variables = {
    DEST_BUCKET = module.s3_bucket_b.s3_bucket_id
  }


  kms_key_arn = aws_kms_key.this.arn

  policy        = aws_iam_policy.lambda_img_metadata.arn
  attach_policy = true

  create_current_version_allowed_triggers = false
  allowed_triggers = {
    S3_trigger = {
      service    = "s3"
      source_arn = module.s3_bucket_a.s3_bucket_arn
    }
  }

  layers = [
    module.lambda_layer_s3.lambda_layer_arn,
  ]


  tags = local.common_tags
  depends_on = [
    module.s3_bucket_a
  ]
}

module "lambda_layer_s3" {
  source = "terraform-aws-modules/lambda/aws"

  create_layer = true

  layer_name          = "${lookup(local.aws-profile, terraform.workspace)}-${lower(lookup(local.workspace, terraform.workspace))}-lambda-img-metadata-layer"
  description         = "Python Lambda Layer with exif, plum and rate limiter"
  compatible_runtimes = ["python3.8"]

  source_path = "${path.module}/lambda_code/layer/"
}
