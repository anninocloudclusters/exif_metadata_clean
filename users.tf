module "user_a" {
  source    = "cloudposse/iam-system-user/aws"
  version   = "0.20.2"
  namespace = "exif_clean"
  stage     = lower(lookup(local.workspace, terraform.workspace))
  name      = "upload_user"

  inline_policies_map = {
    s3 = data.aws_iam_policy_document.s3_policy_user_a.json
  }

  tags = local.common_tags
}

data "aws_iam_policy_document" "s3_policy_user_a" {

  statement {
    sid = "1"

    actions = [
      "s3:PutObject",
      "s3:GetObjectAcl",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObjectAcl"
    ]

    resources = [
      "${module.s3_bucket_a.s3_bucket_arn}/*",
      "${module.s3_bucket_a.s3_bucket_arn}"
    ]

  }


  statement {
    sid = "3"

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]

    resources = [
      "${aws_kms_key.this.arn}"
    ]

  }
}


module "user_b" {
  source    = "cloudposse/iam-system-user/aws"
  version   = "0.20.2"
  namespace = "exif_clean"
  stage     = lower(lookup(local.workspace, terraform.workspace))
  name      = "read_user"

  inline_policies_map = {
    s3 = data.aws_iam_policy_document.s3_policy_user_b.json
  }

  tags = local.common_tags
}

data "aws_iam_policy_document" "s3_policy_user_b" {

  statement {
    sid = "2"

    actions = [
      "s3:GetObjectAcl",
      "s3:GetObject",
      "s3:ListBucket",
    ]

    resources = [
      "${module.s3_bucket_b.s3_bucket_arn}/*",
      "${module.s3_bucket_b.s3_bucket_arn}"
    ]
  }

  statement {
    sid = "3"

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]

    resources = [
      "${aws_kms_key.this.arn}"
    ]

  }
}
