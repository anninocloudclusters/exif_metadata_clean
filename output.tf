output "user_a" {
  sensitive = true
  value     = module.user_a
}
output "user_b" {
  sensitive = true
  value     = module.user_b
}